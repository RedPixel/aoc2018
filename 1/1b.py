a = [int(i) for i in open("1/input")]
c = 0
fs = [0]

while True:
    for i in a:
        c += i
        if c in fs:
            print(c)
            quit()
        else:
            fs.append(c)