twos = 0
threes = 0
for word in open("2/input"):
    for letter in 'qwertyuiopasdfghjklzxcvbnm':
        if word.count(letter) == 2:
            twos += 1
            break
    for letter in 'qwertyuiopasdfghjklzxcvbnm':
        if word.count(letter) == 3:
            threes += 1
            break
print(twos*threes)