words = open("2/input").readlines()

def difference(a,b):
    diff = 0
    for i,l in enumerate(a):
        if b[i] != l:
            diff += 1
    return diff


for w1 in words:
    for w2 in words:
        if w1 != w2:
            if(difference(w1,w2) == 1):
                print(w1)
                print(w2)

# I took the difference between the two string manually, as that is faster than programming it
