import parse
from collections import defaultdict

p = parse.compile("#{:d} @ {:d},{:d}: {:d}x{:d}") # parsing as ints saves 5 casts

f = defaultdict(int)

for claim in open("3/input"):
    (i,x,y,w,h) = p.parse(claim)
    for dx in range(w):
        for dy in range(h):
            f[(x + dx, y + dy)] += 1

overlap = 0
for (_,_),v in f.items():
    if v>1:
        overlap += 1
print(overlap)