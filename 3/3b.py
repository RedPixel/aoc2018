import parse
from collections import defaultdict

p = parse.compile("#{:d} @ {:d},{:d}: {:d}x{:d}") # parsing as ints saves 5 casts

f = defaultdict(int)

doubles = [False] * 1288
doubles[0] = True

for claim in open("3/input"):
    (i,x,y,w,h) = p.parse(claim)
    for dx in range(w):
        for dy in range(h):
            pos = (x + dx, y + dy)
            val = f[pos]
            if val != 0:
                doubles[val] = True
                doubles[i] = True
            else:
                f[pos] = i

print(doubles.index(False))