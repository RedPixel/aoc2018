import parse
from collections import defaultdict

p = parse.compile("[1518-{:d}-{:d} {:d}:{:d}] {}")
p2 = parse.compile("Guard #{:d} begins shift")
s = [line.rstrip() for line in open("4/input")]
s.sort()

time_asleep = defaultdict(int)
sleep_ranges = defaultdict(list)

for line in s:
    (_,_,_,m,action) = p.parse(line)
    if "Guard" in action:
        guard = p2.parse(action)[0]
    if "falls" in action:
        start = m
    elif "wakes" in action:
        end = m
        time_asleep[guard] += end-start
        sleep_ranges[guard].append((start,end))

long_time = max(time_asleep.values())
long_guard = max(time_asleep, key=lambda key: time_asleep[key]) #gets the first key of the max values

mins = [0] * 60

for (start,end) in sleep_ranges[long_guard]:
    for i in range(start,end):
        mins[i] += 1

long_minute = mins.index(max(mins))
print(long_minute * long_guard)
