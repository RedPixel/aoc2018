import parse
from collections import defaultdict

p = parse.compile("[1518-{:d}-{:d} {:d}:{:d}] {}")
p2 = parse.compile("Guard #{:d} begins shift")
s = [line.rstrip() for line in open("4/input")]
s.sort()

sleep_ranges = defaultdict(list)

for line in s:
    (_,_,_,m,action) = p.parse(line)
    if "Guard" in action:
        guard = p2.parse(action)[0]
    if "falls" in action:
        start = m
    elif "wakes" in action:
        end = m
        sleep_ranges[guard].append((start,end))


max_duration = -1

for guard,ranges in sleep_ranges.items():
    mins = [0] * 60
    for (start,end) in ranges:
        for i in range(start,end):
            mins[i] += 1
    duration = max(mins)
    if duration > max_duration:
        max_duration = duration
        max_guard = guard
        max_minute = mins.index(duration)

print(max_guard, max_minute)
print(max_guard * max_minute)
