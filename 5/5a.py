
def collapse(l):
    i = 0
    while i < len(l)-1:
        p = l[i]
        i += 1
        c = l[i]
        if (p!=c and p.lower() == c.lower()):
            del l[i]
            del l[i-1]
            i = 0 if i < 2 else i-2
    return l

input = open("5/input").read().rstrip()
l = list(input)

print(len(collapse(l)))
